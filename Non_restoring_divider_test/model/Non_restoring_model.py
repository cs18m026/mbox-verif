import math
from cocotb.binary import BinaryValue
                 
undefined =  -1
#----------------------------------------------------------------------------------------------------

def signed_div(a,b):    #16
    if (a<0 and b>0):
        quotient = math.ceil(a//b)
    elif (b<0 and a>0):
        quotient = math.ceil(a//b)
    elif a == -(pow(2,63)) and b == -1:
        quotient = a//b
        st = str(bin(quotient))
        st = st.replace('0b','')
        return st

    elif (a<0 and b<0):
        quotient = a//b

    elif a>0 and b>0:
        quotient = math.floor(a//b)

    elif a == 0 and b != 0:
        quotient = 0

    elif (b==0):
        quotient = -1

    return(BinaryValue(value=quotient,bits=64,bigEndian=False,binaryRepresentation=2))


def unsigned_div(a,b):
    if(b==0):
        quotient = undefined
    else:
        quotient = math.floor(a/b)

    if(quotient==undefined):
        return(BinaryValue(value=quotient,bits=64,bigEndian=False,binaryRepresentation=2))
    else:
        return(BinaryValue(value=quotient,n_bits=64,bigEndian=False))


def unsigned_rem(a,b):  #17
    if(b==0):
        remainder = a
    else:
        remainder = (a%b)
        
    # return (a%b)
    # print(remainder)
    # # print(BinaryValue(value=remainder,n_bits=64,bigEndian=False))
    return(BinaryValue(value=remainder,n_bits=64,bigEndian=False))

def signed_rem(a,b):    #16
    if(a<0 and b>0):
        quotient = math.ceil(a/b)
    elif (b<0 and a>0):
        quotient = math.ceil(a/b)
    elif(a<0 and b<0):
        quotient = math.floor(a/b) 
    elif(b!=0):
        return(unsigned_rem(a,b))
    
    if(b==0):
        remainder = a
    elif(a==-(pow(2,63) and b==-1)):
        remainder = 0
    else:
        remainder = a-(b*quotient)
    # print(remainder)
    # print(BinaryValue(value=remainder,n_bits=64,bigEndian=False,binaryRepresentation=1))
    return(BinaryValue(value=remainder,bits=64,bigEndian=False,binaryRepresentation=2))
#----------------------------------------------------------------------------------------------------
