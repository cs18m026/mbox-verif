 # Simple tests for an adder module
import cocotb
from Non_restoring_model import signed_div, unsigned_div, signed_rem, unsigned_rem
import random
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge, ReadOnly, FallingEdge
from cocotb.monitors import Monitor
from cocotb.drivers import BitDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure, TestSuccess




'''
##Opcode bit is 1100 for all the four cases Signed Division, Signed Remainder, Unsigned Division, Unsigned Remainder

##funct3(3 bit input port) bits represents as follows:
    (1). funct3 == 100  for Signed Division
    (2). funct3 == 101  for Unsigned Division
    (3). funct3 == 110  for Signed Remainder
    (4). funct3 == 111  for Unsigned Remainder


'''


@cocotb.test()
def divider_basic_signed_DIV_test(dut):
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("==========Divider Basic Signed Division Test=====================")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    f = open("Fail_Test_case_for_signed_div.txt",'w')
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 14   #fixed..
    for it in range(10):
        print("Iteration=======",it)
        if(it==0):
            A = -15
            B = 5
        elif(it==1):
            A = 10
            B = 0
        elif(it==2):
            A = -(pow(2,63) )
            B = -1
        elif(it==3):
            A = 9223372036854775807
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        elif(it==4):
            A = -9223372036854775808
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        else:
            A = random.randrange(-9223372036854775808,9223372036854775807,1000)
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)

        clkedge = RisingEdge(dut.CLK)
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 0
        dut.RST_N = 0

        for i in range(2):
            yield clkedge

        dut.EN_ma_start = 1
        dut.RST_N = 1
        
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)

        dut.ma_start_opcode = BinaryValue(value=12,bits=4,bigEndian=False)
        dut.ma_start_funct3 = BinaryValue(value=4,bits=3,bigEndian=False)
        yield clkedge
        dut.EN_ma_start = 0
        dut.EN_mav_result = 1
        for i in range(5):
            yield clkedge

        for i in range(62):
            #print("mav_result====",dut.mav_result,"====",i)
            yield clkedge
        
        dutResultBin = "".join(reversed(str(dut.mav_result)))
        valid = dutResultBin[64:65]
        dutResultBin = dutResultBin[:64]
        reversed(dutResultBin)
        modelResultBin = "".join(reversed(str(signed_div(A,B)))) 

        # print(modelResultBin,"<========Model========")
        # print(dutResultBin,"<========DUT========")

        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin and valid=='1'):
            print("Value from DUT and Model are not equal:")
            print(modelResultBin,"<========Model========")
            print(dutResultBin,"<========DUT========")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("Dividend (A) = "+str(A))
            f.write("\nDivisor  (B) = "+str(B))
            f.write("\nOutput from model = "+"".join(reversed(str(modelResultBin))))
            f.write("\nOutput from DUT   = "+"".join(reversed(str(dutResultBin))))
            f.write("\nValid bit from output = "+str(valid))
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.close()
            raise TestFailure("Incorrect DIV result")
        elif(it==0):
            dut.log.info("Basic DIV (-15/5) Test Passed..") 
        elif(it==1):
            dut.log.info("Divide by Zero Signed Division (DIV) Test Passed..") 
        elif(it==2):
            dut.log.info("Sign overflow for DIV Test Passed..") 
        else:
            dut.log.info("OK!")

##===============================Divider Basic Unsigned Division Test=======================================
##==========================================================================================================
@cocotb.test()
def divider_basic_unsigned_div(dut):
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("==========Divider Basic Unsigned Division Test===================")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    f = open("Fail_Test_case_for_usigned_div.txt",'w')
    cocotb.fork(Clock(dut.CLK, 10,).start())
    #divName = 14   #fixed..
    for it in range(10):
        print("Iteration=======",it)
        if(it==0):
            A = 15
            B = 5
        elif(it==1):
            A = 10
            B = 0
        elif(it==2):
            A = (pow(2,63))
            B = 1
        elif(it==3):
            A = 9223372036854775807
            B = random.randrange(0,9223372036854775807,1000)
        elif(it==4):
            A = 1
            B = random.randrange(0,9223372036854775807,1000)
        else:
            A = random.randrange(1,9223372036854775807,1000)
            B = random.randrange(1,9223372036854775807,1000)

        clkedge = RisingEdge(dut.CLK)
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 0
        dut.RST_N = 0

        for i in range(2):
            yield clkedge

        dut.EN_ma_start = 1
        dut.RST_N = 1
        
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)

        dut.ma_start_opcode = BinaryValue(value=12,bits=4,bigEndian=False)
        dut.ma_start_funct3 = BinaryValue(value=5,bits=3,bigEndian=False)
        yield clkedge
        dut.EN_ma_start = 0
        dut.EN_mav_result = 1
        for i in range(5):
            yield clkedge

        for i in range(62):
            #print("mav_result====",dut.mav_result,"====",i)
            yield clkedge
        
        dutResultBin = "".join(reversed(str(dut.mav_result)))
        valid = dutResultBin[64:65]
        dutResultBin = dutResultBin[:64]
        reversed(dutResultBin)
        modelResultBin = "".join(reversed(str(unsigned_div(A,B)))) 

        print(modelResultBin,"<========Model========")
        print(dutResultBin,"<========DUT========")

        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin and valid=='1'):
            print("Value from DUT and Model are not equal:")
            print(A)
            print(B)
            print(modelResultBin,"<========Model========")
            print(dutResultBin,"<========DUT========")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("Dividend (A) = "+str(A))
            f.write("\nDivisor  (B) = "+str(B))
            f.write("\nOutput from model = "+"".join(reversed(str(modelResultBin))))
            f.write("\nOutput from DUT   = "+"".join(reversed(str(dutResultBin))))
            f.write("\nValid bit from dut output = "+str(valid))
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.close()
            raise TestFailure("Incorrect DIV result")
        elif(it==0):
            dut.log.info("Basic DIV (15/5) Test Passed..") 
        elif(it==1):
            dut.log.info("Divide by Zero Signed Division (DIV) Test Passed..") 
        elif(it==2):
            dut.log.info("Sign overflow for DIV Test Passed..")
        else:
            dut.log.info("OK!")

##==========================================================================================================
##==========================================================================================================

##===============================Divider Basic Signed Remainder Test========================================
##==========================================================================================================
@cocotb.test()
def divider_basic_signed_rem(dut):
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("==========Divider Basic Signed Raminder Test=====================")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    f = open("Fail_Test_case_for_signed_remainder.txt",'w')
    cocotb.fork(Clock(dut.CLK, 10,).start())
    #divName = 14   #fixed..
    for it in range(10):
        print("Iteration=======",it)
        if(it==0):
            A = -15
            B = 5
        elif(it==1):
            A = 10
            B = 0
        elif(it==2):
            A = -(pow(2,63) )
            B = -1
        elif(it==3):
            A = 9223372036854775807
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        elif(it==4):
            A = -9223372036854775808
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        else:
            A = random.randrange(-9223372036854775808,9223372036854775807,1000)
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)

        print(A)
        print(B)

        clkedge = RisingEdge(dut.CLK)
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 0
        dut.RST_N = 0

        for i in range(2):
            yield clkedge

        dut.EN_ma_start = 1
        dut.RST_N = 1
        
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)

        dut.ma_start_opcode = BinaryValue(value=12,bits=4,bigEndian=False)
        dut.ma_start_funct3 = BinaryValue(value=6,bits=3,bigEndian=False)
        yield clkedge
        dut.EN_ma_start = 0
        dut.EN_mav_result = 1
        for i in range(5):
            yield clkedge

        for i in range(62):
            #print("mav_result====",dut.mav_result,"====",i)
            yield clkedge
        
        dutResultBin = "".join(reversed(str(dut.mav_result)))
        valid = dutResultBin[64:65]
        dutResultBin = dutResultBin[:64]
        reversed(dutResultBin)
        modelResultBin = "".join(reversed(str(signed_rem(A,B)))) 

        print(modelResultBin,"<========Model========")
        print(dutResultBin,"<========DUT========")

        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin and valid=='1'):
            print("Value from DUT and Model are not equal:")
            print(A)
            print(B)
            print(modelResultBin,"<========Model========")
            print(dutResultBin,"<========DUT========")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("Dividend (A) = "+str(A))
            f.write("\nDivisor  (B) = "+str(B))
            f.write("\nOutput from model = "+"".join(reversed(str(modelResultBin))))
            f.write("\nOutput from DUT   = "+"".join(reversed(str(dutResultBin))))
            f.write("\nValid bit from dut output = "+str(valid))
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.close()
            raise TestFailure("Incorrect DIV result")
        elif(it==0):
            dut.log.info("Basic DIV (-15/5) Test Passed..") 
        elif(it==1):
            dut.log.info("Divide by Zero Signed Division (DIV) Test Passed..") 
        elif(it==2):
            dut.log.info("Sign overflow for DIV Test Passed..") 
        else:
            dut.log.info("OK!")

##==========================================================================================================
##==========================================================================================================


##===============================Divider Basic Unsigned Remainder Test======================================
##==========================================================================================================
@cocotb.test()
def divider_basic_unsigned_remainder_test(dut):
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("==========Divider Basic Unsigned Remainder Test==================")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    print("*****************************************************************")
    f = open("Fail_Test_case_for_usigned_rem.txt",'w')
    cocotb.fork(Clock(dut.CLK, 10,).start())
    #divName = 14   #fixed..
    for it in range(10):
        print("Iteration=======",it)
        if(it==0):
            A = 15
            B = 5
        elif(it==1):
            A = 10
            B = 0
        elif(it==2):
            A = (pow(2,63))
            B = 1
        elif(it==3):
            A = 9223372036854775807
            B = random.randrange(0,9223372036854775807,1000)
        elif(it==4):
            A = 1
            B = random.randrange(0,9223372036854775807,1000)
        else:
            A = random.randrange(1,9223372036854775807,1000)
            B = random.randrange(1,9223372036854775807,1000)

        clkedge = RisingEdge(dut.CLK)
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 0
        dut.RST_N = 0

        for i in range(2):
            yield clkedge

        dut.EN_ma_start = 1
        dut.RST_N = 1
        
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)

        dut.ma_start_opcode = BinaryValue(value=12,bits=4,bigEndian=False)
        dut.ma_start_funct3 = BinaryValue(value=7,bits=3,bigEndian=False)
        yield clkedge
        dut.EN_ma_start = 0
        dut.EN_mav_result = 1
        for i in range(5):
            yield clkedge

        for i in range(62):
            #print("mav_result====",dut.mav_result,"====",i)
            yield clkedge
        
        dutResultBin = "".join(reversed(str(dut.mav_result)))
        valid = dutResultBin[64:65]
        dutResultBin = dutResultBin[:64]
        reversed(dutResultBin)
        modelResultBin = "".join(reversed(str(unsigned_rem(A,B)))) 

        print(modelResultBin,"<========Model========")
        print(dutResultBin,"<========DUT========")

        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin and valid=='1'):
            print("Value from DUT and Model are not equal:")
            print(A)
            print(B)
            print(modelResultBin,"<========Model========")
            print(dutResultBin,"<========DUT========")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("=======================================================================\n")
            f.write("Dividend (A) = "+str(A))
            f.write("\nDivisor  (B) = "+str(B))
            f.write("\nOutput from model = "+"".join(reversed(str(modelResultBin))))
            f.write("\nOutput from DUT   = "+"".join(reversed(str(dutResultBin))))
            f.write("\nValid bit from dut output = "+str(valid))
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.write("\n=======================================================================")
            f.close()
            raise TestFailure("Incorrect DIV result")
        elif(it==0):
            dut.log.info("Basic DIV (15/5) Test Passed..") 
        elif(it==1):
            dut.log.info("Divide by Zero Signed Division (DIV) Test Passed..") 
        elif(it==2):
            dut.log.info("Sign overflow for DIV Test Passed..")
        else:
            dut.log.info("OK!")

##==========================================================================================================
##==========================================================================================================
# #======================================================END================================================